import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class GUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private String input = "input";
    private String output = "Vul de inzendingen in het input veld in. Scheid ze met een enter. \n Vergeet het woord input niet weg te halen :P.";
    private final JTextArea inputfield = new JTextArea(this.input, 5, 10);
    private final JTextArea outputfield = new JTextArea(this.output, 5, 10);
    private final ArrayList<String> needshuffle = new ArrayList();

    private final JButton shuffle = new JButton("Shuffle");

    GUI() {
        this.setTitle("WV shuffle door Gitaar");

        this.setLayout(new FlowLayout());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);

        this.setVisible(true);
        this.init();

    }

    public void init() {

        this.shuffle.setBounds(400, 400, 10, 10);
        this.getSize();
        int w = this.getWidth();
        w = (int) Math.round(0.45 * w);
        int h = this.getHeight();
        h = h - (int) Math.round(0.1 * h);

        this.inputfield.setPreferredSize(new Dimension(w, h));
        this.outputfield.setPreferredSize(new Dimension(w, h));

        this.add(this.inputfield);
        this.add(this.outputfield);
        this.add(this.shuffle);

        this.inputfield.setLineWrap(true);
        this.outputfield.setLineWrap(true);

        // this.input();

        this.Shuffle();
    }

    public void Shuffle() {
        this.shuffle.addActionListener(new ActionListener() {

            // @Override
            @Override
            public void actionPerformed(final ActionEvent e) {
                GUI.this.needshuffle.clear();
                GUI.this.input = GUI.this.inputfield.getText();
                @SuppressWarnings("resource")
                final Scanner scan = new Scanner(GUI.this.input);
                int i = 0;
                while (scan.hasNextLine()) {
                    GUI.this.needshuffle.add(scan.nextLine());
                    i++;
                }
                Collections.shuffle(GUI.this.needshuffle);
                GUI.this.output = "";
                for (i = 0; i < GUI.this.needshuffle.size() - 1; i++) {
                    GUI.this.output += GUI.this.needshuffle.get(i) + "\n";
                }
                GUI.this.output += GUI.this.needshuffle
                        .get(GUI.this.needshuffle.size() - 1);
                GUI.this.appendNewText(GUI.this.output);
            }
        });
    }

    public static void main(final String[] args) {
        final JFrame frame = new GUI();
    }

    public void appendNewText(final String txt) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                GUI.this.outputfield.setText(txt);
                // outputText.setText(outputText.getText + "\n"+ txt); Windows
                // LineSeparator
            }
        });
    }
}
