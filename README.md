# README #

Dit is een shufflelaar geschreven in java.

### Preview! ###

![1.0.0.png](https://bitbucket.org/repo/RnyrMX/images/1743578947-1.0.0.png)

### Hoe te gebruiken? ###

Ga naar downloads.
En klik op wvshufflelaar.jar.
Download hem en voer hem uit.

### Wat heb je ervoor nodig ###

Java moet geinstaleerd zijn op je computer.
Maar meestal is dat wel het geval,zo niet [download](https://java.com/nl/download/)
